# MAC0110 - MiniEP7
# Guilherme de Barros Monteiro Ferreira - 11208590

function sin(x)
    n = 1
    sinal = 1
    sum = 0
    while n <= 19
        aux = BigFloat((x ^ n) / factorial(BigInt(n)))
        sum = BigFloat(sum + (aux * sinal))
        n = n + 2
        sinal = sinal * (-1)
    end
    return sum
end

function cos(x)
    n = 0
    sinal = 1
    sum = 0
    while n <= 20
        aux = BigFloat((x ^ n) / factorial(BigInt(n)))
        sum = BigFloat(sum + (aux * sinal))
        n = n + 2
        sinal = sinal * (-1)
    end
    return sum
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

function tan(x)
    n = 2
    sum = x
    while n <= 11
        aux = BigFloat(((2 ^ (2 * n)) * ((2 ^ (2 * n)) - 1) * bernoulli(n) * (x ^ ((2 * n) - 1)))/factorial(BigInt(2 * n)))
        sum = BigFloat(sum + aux)
        n = n + 1
    end
    return sum
end

function check_sin(value, x)
    erro = 0.0001
    if abs(value - sin(x)) <= erro
        return true
    else
        return false
    end
end

function check_cos(value, x)
    erro = 0.0001
    if abs(value - cos(x)) <= erro
        return true
    else
        return false
    end
end

function check_tan(value, x)
    erro = 0.0001
    if x == pi
        return value ≈ 0
    elseif abs(x) < BigFloat(pi/2)
        if abs(value -(tan(x))) <= erro
            return true
        else
            return false
        end
    else
        if abs(value - BigFloat(sin(x)/cos(x))) <= erro
            return true
        else
            return false
        end
    end
end

using Test
function test()
    erro = 0.0001
    @test abs(sin(0) - (0)) <= erro
    @test abs(sin(pi / 4) - (sqrt(2) / 2)) <= erro
    @test abs(sin((3 * pi) / 4) - (sqrt(2) / 2)) <= erro
    @test abs(cos(0) - (1)) <= erro
    @test abs(cos(pi / 4) - (sqrt(2) / 2)) <= erro
    @test abs(cos((3 * pi) / 4) - ((-1) * (sqrt(2) / 2))) <= erro
    @test abs(tan(0) - (0)) <= erro
    @test abs(tan(pi / 6) - (sqrt(3) / 3)) <= erro
    @test abs(tan(pi / 4) - (1)) <= erro
    @test check_sin(1 / 2, pi / 6) == true
    @test check_sin(sqrt(3) / 2, pi / 3) == true
    @test check_cos(sqrt(3) / 2, pi / 6) == true
    @test check_cos(1 / 2, pi / 3) == true
    @test check_cos(-1, pi) == true
    @test check_tan(sqrt(3) / 3, pi / 6) == true
    @test check_tan(0, pi) == true
    println("Final dos testes")
end

test()

function taylor_sin(x)
    n = 1
    sinal = 1
    sum = 0
    while n <= 19
        aux = BigFloat((x ^ n) / factorial(BigInt(n)))
        sum = BigFloat(sum + (aux * sinal))
        n = n + 2
        sinal = sinal * (-1)
    end
    return sum
end

function taylor_cos(x)
    n = 0
    sinal = 1
    sum = 0
    while n <= 20
        aux = BigFloat((x ^ n) / factorial(BigInt(n)))
        sum = BigFloat(sum + (aux * sinal))
        n = n + 2
        sinal = sinal * (-1)
    end
    return sum
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

function taylor_tan(x)
    n = 2
    sum = x
    while n <= 11
        aux = BigFloat(((2 ^ (2 * n)) * ((2 ^ (2 * n)) - 1) * bernoulli(n) * (x ^ ((2 * n) - 1)))/factorial(BigInt(2 * n)))
        sum = BigFloat(sum + aux)
        n = n + 1
    end
    return sum
end

function check_sin(value, x)
    erro = 0.0001
    if abs(value - taylor_sin(x)) <= erro
        return true
    else
        return false
    end
end

function check_cos(value, x)
    erro = 0.0001
    if abs(value - taylor_cos(x)) <= erro
        return true
    else
        return false
    end
end

function check_tan(value, x)
    erro = 0.0001
    if x == pi
        return value ≈ 0
    elseif abs(x) < BigFloat(pi/2)
        if abs(value -(taylor_tan(x))) <= erro
            return true
        else
            return false
        end
    else
        if abs(value - BigFloat(taylor_sen(x)/taylor_cos(x))) <= erro
            return true
        else
            return false
        end
    end
end
